(function() {
    const parseHTML = (node,  cb) => {
        cb(node)
        
        let nextNode = node.firstChild
    
        while (nextNode !== null) {
            parseHTML(nextNode, cb)
            nextNode = nextNode.nextSibling
        }
    }

    const regex = /\b(\d+) ?(°?|degrees?) ?(F|Fahrenheit)\b/g

    const nodes = []
    
    parseHTML(document.body, node => {
        const text = node.textContent.trim()
        const children = node.children || ['text']
        
        if (children.length > 0 || text === "") {
            return
        }
        
        if (regex.test(text) === false) {
            return false
        }

        nodes.push(node)
    })

    nodes.forEach(node => {
        node.innerHTML = node.textContent
            .trim()
            .replace(regex, (match, f) => {
                const c = ((+f) - 32) * 5/9
                return `<span class="ftoc" data-value="${c.toFixed(2)} °C">${match}</span>`
            })
    })
})()