Extension for Google Chrome which converts degrees in Fahrenheit to Celsius just by hover over a number with degrees Fahrenheit.

Motivation came when I was searching the web for culinary recipes and majority of sites specify temperature as a degrees Fahrenheit but my native measurement for temperature is degrees Celsius.

You won't find it in Google Web Store just yet, but you can load it as unpacked extension in developer mode ([instructions](https://developer.chrome.com/extensions/getstarted#unpacked)).